//
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"time"

	"bitbucket.org/uwaploe/rdadc/internal/config"
	"bitbucket.org/uwaploe/rdadc/internal/sampler"
	"bitbucket.org/uwaploe/tsfpga/adc"
	pb "bitbucket.org/uwaploe/tsfpga/api"
	tea "github.com/charmbracelet/bubbletea"
	"google.golang.org/grpc"
	"periph.io/x/conn/v3/analog"
)

const Usage = `Usage: adcmon [options] [cfgfile]

Text-mode UI to monitor DP A/D channels.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	dumpCfg = flag.Bool("dumpcfg", false,
		"Dump default configuration to stdout and exit")
	rpcAddr      string        = "localhost:10101"
	tickInterval time.Duration = time.Second
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.DurationVar(&tickInterval, "interval", tickInterval,
		"ADC sampling interval")
	flag.StringVar(&rpcAddr, "rpc-addr", rpcAddr,
		"host:port for the TS-FPGA gRPC server")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpCfg {
		cfg, _ := config.InitConfig("")
		cfg.Dump(os.Stdout)
		os.Exit(0)
	}

	return flag.Args()
}

func grpcConnect(addr string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())

	return grpc.Dial(addr, opts...)
}

func main() {
	_ = parseCmdLine()

	cfg, err := config.InitConfig(flag.Arg(0))
	if err != nil {
		log.Fatalf("Cannot parse config file: %v", err)
	}

	gconn, err := grpcConnect(rpcAddr)
	if err != nil {
		log.Fatalf("Cannot access TS-FPGA server: %v", err)
	}

	dev, err := adc.New(pb.NewFpgaMemClient(gconn))
	if err != nil {
		log.Fatal(err)
	}

	err = dev.Configure(adc.Size16Bits, adc.GainX1, adc.AnSelPin77)
	if err != nil {
		log.Fatal(err)
	}

	pins := make([]analog.PinADC, 0)
	for _, i := range cfg.Indicies() {
		p, err := dev.PinForChannel(i)
		if err != nil {
			log.Fatal(err)
		}
		pins = append(pins, p)
	}

	s := sampler.New(pins)
	p := tea.NewProgram(
		initModel(cfg, s, tickInterval),
		tea.WithAltScreen())

	if _, err := p.Run(); err != nil {
		log.Println(err)
	}

}
