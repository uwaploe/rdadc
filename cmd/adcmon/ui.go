package main

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/uwaploe/rdadc/internal/config"
	"bitbucket.org/uwaploe/rdadc/internal/sampler"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

type modelStyles struct {
	labelStyle lipgloss.Style
	textStyle  lipgloss.Style
	hdrStyle   lipgloss.Style
}

func (m modelStyles) renderValue(key, val string) string {
	return lipgloss.JoinHorizontal(lipgloss.Top, m.labelStyle.Render(key),
		m.textStyle.Render(val))
}

type tickMsg time.Time

func tick(interval time.Duration) tea.Cmd {
	return tea.Tick(interval, func(t time.Time) tea.Msg {
		return tickMsg(t)
	})
}

type model struct {
	channels []config.Channel
	funcs    []config.ScaleFunc
	rec      []sampler.Value
	t        time.Time
	s        *sampler.Sampler
	dt       time.Duration
	modelStyles
}

func initModel(cfg config.ProgCfg, s *sampler.Sampler, dt time.Duration) model {
	m := model{
		channels: cfg.Channels,
		funcs:    cfg.Funcs(),
		s:        s,
		dt:       dt,
	}

	m.modelStyles.labelStyle = lipgloss.NewStyle().Bold(true).Width(30)
	m.modelStyles.textStyle = lipgloss.NewStyle().PaddingLeft(4)
	m.modelStyles.hdrStyle = lipgloss.NewStyle().
		MarginLeft(1).
		MarginRight(5).
		MarginBottom(1).
		Foreground(lipgloss.Color("#FFD000")).
		Background(lipgloss.Color("#6124DF")).
		Padding(0, 1)
	return m
}

func (m model) Init() tea.Cmd {
	return tick(m.dt)
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			return m, tea.Quit
		}
	case tickMsg:
		m.t = time.Time(msg)
		m.rec = m.s.Sample()
		return m, tick(m.dt)
	}

	return m, nil
}

const timeFormat = "2006-01-02 15:04:05"

func (m model) View() string {
	var b strings.Builder
	b.WriteString(m.hdrStyle.Render("A/D Monitor") + "\n\n")
	b.WriteString(m.renderValue("Time:", m.t.Format(timeFormat)) + "\n")
	for i, val := range m.rec {
		cvt := m.funcs[i]
		key := fmt.Sprintf("%s (%s)", m.channels[i].Name, m.channels[i].Units)
		if m.channels[i].UseVolts {
			b.WriteString(m.renderValue(key,
				fmt.Sprintf("%.3f", cvt(val.V))) + "\n")
		} else {
			b.WriteString(m.renderValue(key,
				fmt.Sprintf("%.3f", cvt(float32(val.Raw)))) + "\n")
		}
	}
	b.WriteString("\n\n q/ctrl+c: quit\n")
	return b.String()
}
