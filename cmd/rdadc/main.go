// Read the ADC on a TS-4200 or TS-4800 CPU board.
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/rdadc/internal/config"
	"bitbucket.org/uwaploe/rdadc/internal/sampler"
	"bitbucket.org/uwaploe/tsfpga/adc"
	pb "bitbucket.org/uwaploe/tsfpga/api"
	"google.golang.org/grpc"
	"periph.io/x/conn/v3/analog"
)

const Usage = `Usage: rdadc [options] [cfgfile]

Sample ADC channels on a TS-4200 or TS-4800 CPU board
and stream the values to standard output.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	dumpCfg = flag.Bool("dumpcfg", false,
		"Dump default configuration to stdout and exit")
	rpcAddr      string        = "localhost:10101"
	tickInterval time.Duration = time.Second
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.DurationVar(&tickInterval, "interval", tickInterval,
		"ADC sampling interval")
	flag.StringVar(&rpcAddr, "rpc-addr", rpcAddr,
		"host:port for the TS-FPGA gRPC server")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpCfg {
		cfg, _ := config.InitConfig("")
		cfg.Dump(os.Stdout)
		os.Exit(0)
	}

	return flag.Args()
}

func grpcConnect(addr string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())

	return grpc.Dial(addr, opts...)
}

func timestamp(t time.Time) []interface{} {
	tt := t.Truncate(time.Microsecond)
	return []interface{}{tt.Unix(), int64(tt.Nanosecond()) / 1000}
}

func showRec(t0 time.Time, funcs []config.ScaleFunc, useVolts []bool, vals []sampler.Value) {
	fmt.Printf("%d,%d", timestamp(t0)...)
	for i, v := range vals {
		if useVolts[i] {
			fmt.Printf(",%.3f", funcs[i](v.V))
		} else {
			fmt.Printf(",%.3f", funcs[i](float32(v.Raw)))
		}
	}
	fmt.Println("")
}

func main() {
	_ = parseCmdLine()

	cfg, err := config.InitConfig(flag.Arg(0))
	if err != nil {
		log.Fatalf("Cannot parse config file: %v", err)
	}

	gconn, err := grpcConnect(rpcAddr)
	if err != nil {
		log.Fatalf("Cannot access TS-FPGA server: %v", err)
	}

	dev, err := adc.New(pb.NewFpgaMemClient(gconn))
	if err != nil {
		log.Fatal(err)
	}

	err = dev.Configure(adc.Size16Bits, adc.GainX1, adc.AnSelPin77)
	if err != nil {
		log.Fatal(err)
	}

	pins := make([]analog.PinADC, 0)
	for _, i := range cfg.Indicies() {
		p, err := dev.PinForChannel(i)
		if err != nil {
			log.Fatal(err)
		}
		pins = append(pins, p)
	}

	s := sampler.New(pins)
	funcs := cfg.Funcs()
	useVolts := make([]bool, len(cfg.Channels))
	for i, channel := range cfg.Channels {
		useVolts[i] = channel.UseVolts
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	header := []string{"seconds", "microseconds"}
	header = append(header, cfg.Names()...)
	fmt.Println(strings.Join(header, ","))

	ticker := time.NewTicker(tickInterval)
	for {
		select {
		case t0 := <-ticker.C:
			showRec(t0, funcs, useVolts, s.Sample())
		case <-sigs:
			return
		}
	}
}
