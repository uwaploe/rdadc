package config

import (
	"io"
	"os"

	yaml "gopkg.in/yaml.v2"
)

// Default A/D channel configuration
var defaultCfg = ProgCfg{
	Channels: []Channel{
		{Name: "Ain3", Cnum: 3, Units: "counts", C: []float32{0, 1}},
		{Name: "Ain4", Cnum: 4, Units: "counts", C: []float32{0, 1}},
		{Name: "Ain5", Cnum: 5, Units: "counts", C: []float32{0, 1}},
		{Name: "Ain6", Cnum: 6, Units: "counts", C: []float32{0, 1}},
	},
}

type ScaleFunc func(float32) float32

// Return a function that performs the polynomial multiplication
//   y = C[0] + C[1]*x + C[2]*x^2 + ...
func polyMult(c []float32) ScaleFunc {
	return ScaleFunc(func(x float32) float32 {
		y := float32(0)
		for i := len(c) - 1; i > 0; i-- {
			y = x * (c[i] + y)
		}
		return y + c[0]
	})
}

type Channel struct {
	Name     string    `yaml:"name"`
	Cnum     uint      `yaml:"cnum"`
	Units    string    `yaml:"units"`
	UseVolts bool      `yaml:"use_volts"`
	C        []float32 `yaml:"c,flow"`
}

type ProgCfg struct {
	Channels []Channel `yaml:"channels"`
}

func InitConfig(path string) (ProgCfg, error) {
	cfg := defaultCfg
	if path != "" {
		contents, err := os.ReadFile(path)
		if err != nil {
			return cfg, err
		}
		err = cfg.Load(contents)
		if err != nil {
			return cfg, err
		}
	}
	return cfg, nil
}

func (cfg *ProgCfg) Indicies() []uint {
	idx := make([]uint, 0, len(cfg.Channels))
	for _, c := range cfg.Channels {
		idx = append(idx, c.Cnum-1)
	}

	return idx
}

func (cfg *ProgCfg) Names() []string {
	names := make([]string, 0, len(cfg.Channels))
	for _, c := range cfg.Channels {
		names = append(names, c.Name)
	}

	return names
}

func (cfg *ProgCfg) Funcs() []ScaleFunc {
	f := make([]ScaleFunc, 0, len(cfg.Channels))
	for _, c := range cfg.Channels {
		f = append(f, polyMult(c.C))
	}

	return f
}

func (c *ProgCfg) Load(contents []byte) error {
	return yaml.Unmarshal(contents, c)
}

func (c ProgCfg) Dump(w io.Writer) error {
	enc := yaml.NewEncoder(w)
	enc.Encode(c)
	return enc.Close()
}
