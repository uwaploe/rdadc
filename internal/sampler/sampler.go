// Package sampler provides an interface to a set of ADC channels
package sampler

import (
	"periph.io/x/conn/v3/analog"
	"periph.io/x/conn/v3/physic"
)

type Sampler struct {
	pins []analog.PinADC
}

type Value struct {
	V   float32 `json:"v"`
	Raw uint16  `json:"counts"`
}

func New(pins []analog.PinADC) *Sampler {
	s := &Sampler{}
	s.pins = make([]analog.PinADC, len(pins))
	copy(s.pins, pins)
	return s
}

func (s *Sampler) Sample() []Value {
	rec := make([]Value, len(s.pins))
	for i, p := range s.pins {
		samp, _ := p.Read()
		rec[i] = Value{V: float32(samp.V) / float32(physic.Volt),
			Raw: uint16(samp.Raw)}
	}
	return rec
}
